<?php namespace Keios\PGPaySafeCard;

use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use System\Classes\PluginBase;
use Event;

/**
 * PG-PayU-RO Plugin Information File
 *
 * @package Keios\PGPayURO
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PG-PaySafeCard',
            'description' => 'keios.pgpaysafecard::lang.labels.pluginDesc',
            'author'      => 'Keios',
            'icon'        => 'icon-dollar',
        ];
    }

    public function register()
    {
        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PGPaySafeCard\Operators\PaySafeCard');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'paysafecard.general'           => [
                            'label' => 'keios.pgpaysafecard::lang.settings.general',
                            'tab'   => 'keios.pgpaysafecard::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'paysafecard.info'              => [
                            'type' => 'partial',
                            'path' => '$/keios/pgpaysafecard/partials/_paysafecard_info.htm',
                            'tab'  => 'keios.pgpaysafecard::lang.settings.tab',
                        ],
                        'paysafecard.soap_api_username' => [
                            'label'   => 'keios.pgpaysafecard::lang.settings.soap_api_username',
                            'tab'     => 'keios.pgpaysafecard::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                        'paysafecard.soap_api_password' => [
                            'label'   => 'keios.pgpaysafecard::lang.settings.soap_api_password',
                            'tab'     => 'keios.pgpaysafecard::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                        'paysafecard.mrt_id_eur' => [
                            'label'   => 'keios.pgpaysafecard::lang.settings.mrt_id_eur',
                            'tab'     => 'keios.pgpaysafecard::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',

                        ],
                        'paysafecard.mrt_id_usd' => [
                            'label'   => 'keios.pgpaysafecard::lang.settings.mrt_id_usd',
                            'tab'     => 'keios.pgpaysafecard::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',

                        ],

                        'paysafecard.mode'              => [
                            'label'   => 'keios.pgpaysafecard::lang.settings.testMode',
                            'tab'     => 'keios.pgpaysafecard::lang.settings.tab',
                            'type'    => 'dropdown',
                            'options' => [
                                'test' => 'Test',
                                'live' => 'Live',
                            ],
                        ],
                        'paysafecard.debug'             => [
                            'label'   => 'keios.pgpaysafecard::lang.settings.testdebugMode',
                            'tab'     => 'keios.pgpaysafecard::lang.settings.tab',
                            'type'    => 'switch',
                            'default' => false,
                        ],
                        'paysafecard.autocorrect'       => [
                            'label'   => 'keios.pgpaysafecard::lang.settings.autocorrect',
                            'tab'     => 'keios.pgpaysafecard::lang.settings.tab',
                            'type'    => 'switch',
                            'default' => false,
                        ],
                    ]
                );
            }
        );
    }
}