<?php
/**
 * Created by Keios Solutions
 * User: Jakub Zych
 * Date: 2/9/16
 * Time: 2:13 PM
 */

namespace Keios\PGPaySafeCard\Classes;

use Keios\MoneyRight\Money;
use Keios\PaymentGateway\Core\Operator;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PaymentGateway\ValueObjects\Details;
use Illuminate\Support\Facades\Log;
use JMS\Serializer\Tests\Fixtures\Order;
use Keios\PaymentGateway\Contracts\Orderable;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PGPaySafeCard\Operators\PaySafeCard;

/**
 * Class PaySafeChargeBuilder
 * @package Keios\PGPaySafeCard\Classes
 */
class PaySafeChargeBuilder
{
    use SettingsDependent;

    /**
     * @var Orderable
     */
    protected $cart;

    /**
     * @var Details
     */
    protected $details;

    /**
     * @var Operator
     */
    protected $payment;

    /**
     * PayURoConnector constructor.
     *
     * @param Operator  $payment
     */
    public function __construct(Operator $payment)
    {
        $this->payment = $payment;
    }


    /**
     *
     * @return SOPGClassicMerchantClient
     */
    public function buildClient()
    {

        $this->getSettings();
        $sysLang = 'en';

        $mode = $this->getSettings()->get('paysafecard.mode');
        $debug = $this->getSettings()->get('paysafecard.debug');
        $autoCorrect = $this->getSettings()->get('paysafecard.autocorrect');

        $client = new SOPGClassicMerchantClient($debug, $sysLang, $autoCorrect, $mode);

        return $client;

    }

    /**
     * @param PaySafeCard $data
     *
     * @return SOPGClassicMerchantClient
     */
    public function buildPayment($data)
    {
        $this->getSettings();

        $url = \URL::to(
            '_paymentgateway/'.urlencode(OperatorUrlizer::urlize($this->payment)).
            '?pgUuid='.base64_encode($this->payment->uuid)
        );

       $currency = $this->payment->order->cart->getTotalNetCost()->getCurrency()->getIsoCode();
       $email = $this->payment->order->user->email;
       $emailEncoded = md5($email);
        // <---- Fixed values ---->
        $username = $this->getSettings()->get('paysafecard.soap_api_username');
        $password = $this->getSettings()->get('paysafecard.soap_api_password');

/* todo remove
        if($currency == 'EUR') {
            $merchantClientId = $this->getSettings()->get('paysafecard.mrt_id_eur'); //todo - multicurrency
        } else {
            $merchantClientId = $this->getSettings()->get('paysafecard.mrt_id_usd');
        }
*/ 
        $mode = $this->getSettings()->get('paysafecard.mode');

        $currency = $data->amount->getCurrency()->getIsoCode();
        $amount = $data->amount->getAmountBasic();
        $merchantClientId = $emailEncoded;
        if($mode == 'test'){        
           $merchantClientId = 'myClientI2d';
        }

        //$merchantClientId = '1000010720';
        $okUrl = $url.'&status=ok&mtid='.$this->payment->uuid.'&currency='.$currency.'&amount='.$amount;
        $nokUrl = $url.'&status=nok&mtid='.$this->payment->uuid.'&currency='.$currency.'&amount='.$amount;
        $pnUrl = $url.'&status=pn&mtid='.$this->payment->uuid.'&currency='.$currency.'&amount='.$amount;

        $data->client->merchant($username, $password);
        $data->client->setCustomer($amount, $currency, $data->uuid, $merchantClientId);
        $data->client->setUrl($okUrl, $nokUrl, $pnUrl);

        /*
        $status = $client->getSerialNumbers($uuid, $currency, $subId = '');
        if ($status === 'execute') {
            $execute = $client->executeDebit($amount, '1');
            if ($execute === true) {
                // here user account topup -EXECUTE DEBIT SUCCESSFUL- !!!
                $this->logDebug($client);
            }
        }
        */

        return $data->client;
    }

    /**
     * @param SOPGClassicMerchantClient $client
     *
     * @return SOPGClassicMerchantClient
     */
    public function executePayment($client)
    {
        $paymentPanel = $client->createDisposition();

        if ($paymentPanel == false) {
            $this->logDebug($client);
        } else {
            $client->data['redirect_url'] = $paymentPanel;

            return $client;
        }
    }


    /**
     * @param SOPGClassicMerchantClient $client
     */
    public function logDebug($client)
    {
        $error = $client->getLog('error');
        \Log::debug($error);
    }

    /**
     * @return string
     */
    private function full_url()
    {
        $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
        $sp = strtolower($_SERVER["SERVER_PROTOCOL"]);
        $protocol = substr($sp, 0, strpos($sp, "/")).$s;

        return $protocol."://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
    }

}