<?php namespace Keios\PGPaySafeCard\Operators;

use Finite\StatefulInterface;
use Keios\PaymentGateway\Exceptions\CancellationFailureException;
use Keios\PaymentGateway\Exceptions\AcceptanceFailureException;
use Keios\PaymentGateway\Exceptions\RefundFailureException;
use Keios\PaymentGateway\Exceptions\RejectionFailureException;
use Keios\PGPaySafeCard\Classes\PaySafeChargeBuilder;
use Keios\PGPaySafeCard\Classes\SOPGClassicMerchantClient;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Symfony\Component\HttpFoundation\Response;
use Keios\PaymentGateway\Core\Operator;


/**
 * Class PayU
 *
 * @package Keios\PGPayURO
 */
class PaySafeCard extends Operator implements StatefulInterface
{
    use SettingsDependent;

    const CREDIT_CARD_REQUIRED = false;

    /**B
     * @var string
     */
    public static $operatorCode = 'keios.pgpaysafecard::lang.operators.paysafecard';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgpaysafecard/assets/img/paysafecard/logo.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    public $client;

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {
        $builder = new PaySafeChargeBuilder($this);
        $this->client = $builder->buildClient();
        $this->client = $builder->buildPayment($this);
        $this->client = $builder->executePayment($this->client);
        $redirectUrl = $this->client->data['redirect_url'];

        return new PaymentResponse($this, $redirectUrl);
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function processNotification(array $data)
    {
\Log::info(print_r($data, true));
        if ($data['status'] == 'pn'||$data['status'] == 'ok') {

            $this->validateResponse($data);
            if($data['status'] == 'pn'){
                ob_end_clean();
                ignore_user_abort(true); // just to be safe 
                $response = \Response::json(['status' => 'ok'], 200)->header('Connection', 'close');
                $response->header('Content-Length', strlen($response->getContent()));
                $response->send();
            }
            try {
                $builder = new PaySafeChargeBuilder($this);
                $this->client = $builder->buildClient();
                $this->client = $builder->buildPayment($this);
                $status = $this->client->getSerialNumbers($data['mtid'], $data['currency'], '');
                \Log::info(print_r($status, true));
                if ($status === 'execute') {
                    $this->client->executeDebit($this->amount->getAmountBasic(), '1');
                    $this->accept();
                } elseif ( $status === 'cancelled' ) {
                    $this->cancel();
                } elseif ( $status === 'invalid' ) {
                    $this->rejectFromApi();
                }
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
            }

        //    exit();
        }
        if ($data['status'] == 'nok') {
            $this->cancel();
        }

        return \Redirect::to($this->returnUrl);
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {

    }

    /**
     * @param $data
     *
     * @throws \Exception
     */
    public function validateResponse($data)
    {
        $rules = [
            'status'   => 'required',
            'currency' => 'required',
            'amount'   => 'required',
            'mtid'     => 'required',
        ];

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            throw new \Exception('Problem with Payment Provider. Please try again in a moment.');
        }
    }

    /**
     * @param array $data
     *
     * @return integer
     * @throws \Exception
     */
    public static function extractUuid(array $data)
    {
        if (isset($data['pgUuid'])) {
            return base64_decode($data['pgUuid']);
        } else {
            throw new \Exception('Invalid payment data - uuid was not found.');
        }

    }

}